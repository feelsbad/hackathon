package com.hackathon.service

import io.dropwizard.Configuration
import io.dropwizard.client.JerseyClientConfiguration

class Configuration(
    val project: String,
    val env: String,
    val jerseyClient: JerseyClientConfiguration = JerseyClientConfiguration().apply {
        this.isGzipEnabled = false
    }
) : Configuration()

class MongoConfiguration(
    val connectionString: String,
    val database: String
)