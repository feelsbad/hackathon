package com.hackathon.service.api

import com.google.inject.Inject
import com.hackathon.service.service.MainService
import io.swagger.annotations.Api
import org.slf4j.LoggerFactory
import javax.validation.constraints.NotNull
import javax.ws.rs.Consumes
import javax.ws.rs.DELETE
import javax.ws.rs.GET
import javax.ws.rs.PUT
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.QueryParam

const val PROFILE_ID_HEADER = "X-Profile-Id"

@Api(value = "/v1", description = "")
@Path("/v1")
@Produces("application/json")
@Consumes("application/json")
class MainResource @Inject constructor(
    private val service: MainService
) {

    private val logger = LoggerFactory.getLogger(this.javaClass)

    @PUT
    @Path("/room/{userId}")
    fun getRoom(
        @NotNull @PathParam("userId")
        userId: String
    ) = service.getRoom(userId)

    @DELETE
    @Path("/room/{userId}")
    fun exitRoom(
        @NotNull @PathParam("userId")
        userId: String
    ) = service.exitRoom(userId)

    @PUT
    @Path("/top/{userId}/score")
    fun setScore(
        @NotNull @PathParam("userId")
        userId: String,
        @NotNull @QueryParam("nickname")
        nickname: String,
        @NotNull @QueryParam("score")
        score: Int
    ) {
        service.setScore(userId, score, nickname)
    }

    @GET
    @Path("/top")
    fun getTop() = service.getTop()

    @GET
    @Path("/rooms")
    fun getRooms() = service.getRooms()

    @DELETE
    @Path("/cleanup")
    fun cleanup() = service.cleanup()
}