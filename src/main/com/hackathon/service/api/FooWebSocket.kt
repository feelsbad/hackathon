package com.hackathon.service.api

import javax.websocket.OnClose
import javax.websocket.OnMessage
import javax.websocket.OnOpen
import javax.websocket.Session
import javax.websocket.server.ServerEndpoint

/*
 *  Copyright (C) MyTona, LLC - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Mhs <maksim.barashkov@mytona.com> on 16/11/2019.
 */

@ServerEndpoint("/foo")
class FooWebSocket {
    @OnOpen
    fun onOpen(session: Session) {
    }

    @OnMessage
    fun onMessage(session: Session, message: String) {
    }

    @OnClose
    fun onClose(session: Session) {
    }
}