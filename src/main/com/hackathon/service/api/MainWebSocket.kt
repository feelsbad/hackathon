package com.hackathon.service.api

import com.hackathon.service.common.RoomID
import com.hackathon.service.common.UserID
import com.hackathon.service.controller.StateMutator
import com.hackathon.service.model.Attack
import com.hackathon.service.model.BOLT_MS
import com.hackathon.service.model.BOLT_WIDTH
import com.hackathon.service.model.CROSSBOW_BOLT_COUNT
import com.hackathon.service.model.CROSSBOW_RESPAWN_POINT
import com.hackathon.service.model.CROSSBOW_RESPAWN_TIME
import com.hackathon.service.model.Connected
import com.hackathon.service.model.CrossBowPick
import com.hackathon.service.model.CrossBowRespawn
import com.hackathon.service.model.Direction
import com.hackathon.service.model.Disconnected
import com.hackathon.service.model.EPSILON
import com.hackathon.service.model.MAGIC_PING
import com.hackathon.service.model.MatchFinished
import com.hackathon.service.model.Message
import com.hackathon.service.model.MessageDecoder
import com.hackathon.service.model.MessageEncoder
import com.hackathon.service.model.PLAYER_ATTACK_RANGE
import com.hackathon.service.model.PLAYER_HEIGHT
import com.hackathon.service.model.PLAYER_MS
import com.hackathon.service.model.PLAYER_RESPAWN_TIME
import com.hackathon.service.model.PLAYER_WIDTH
import com.hackathon.service.model.PlayerAttacked
import com.hackathon.service.model.PlayersKilled
import com.hackathon.service.model.PlayersRespawn
import com.hackathon.service.model.PointsReceived
import com.hackathon.service.model.Position
import com.hackathon.service.model.PositionChanged
import com.hackathon.service.model.RESPAWN_POINTS
import com.hackathon.service.model.ROOM_MAX_SCORE
import com.hackathon.service.model.Room
import com.hackathon.service.model.State
import com.hackathon.service.model.StateUpdate
import com.hackathon.service.model.User
import com.hackathon.service.model.Weapon
import com.hackathon.service.model.aabb
import com.hackathon.service.model.huyabb
import com.hackathon.service.model.isBeaten
import com.hackathon.service.singleton.ServerState
import com.hackathon.service.singleton.roomIsEmpty
import org.slf4j.LoggerFactory
import java.util.Timer
import javax.websocket.OnClose
import javax.websocket.OnMessage
import javax.websocket.OnOpen
import javax.websocket.Session
import javax.websocket.server.PathParam
import javax.websocket.server.ServerEndpoint
import javax.ws.rs.BadRequestException
import kotlin.concurrent.schedule
import kotlin.math.abs
import kotlin.math.atan2
import kotlin.math.pow
import kotlin.math.sqrt

@ServerEndpoint(
    "/ws/{userId}",
    encoders = [MessageEncoder::class],
    decoders = [MessageDecoder::class]
)
class MainWebSocket {
    private val logger = LoggerFactory.getLogger(this::class.java)
    private val mutator = StateMutator()

    private fun broadcast(roomID: RoomID, message: Message) {
        val room = ServerState.rooms[roomID]
        if (room?.running == false && message.event !is MatchFinished)
            return

        val userStates = room?.users
            ?.map { (user, state) -> user.userId to state }?.toMap()
        val msg = Message("server", roomID, StateUpdate(userStates))

        ServerState.rooms[roomID]?.users
            ?.forEach { (user, _) ->
                user.session.asyncRemote.sendObject(message)
                user.session.asyncRemote.sendObject(msg)
            }
    }

    @OnOpen
    fun onOpen(
        session: Session,
        @PathParam("userId") userId: String
    ) {
        val roomId = ServerState.mainService?.getRoom(userId)!!._id
        when (ServerState.rooms.containsKey(roomId)) {
            true ->
                ServerState.rooms[roomId]?.also { room ->
                    room.users.put(User(userId, session), State(skin = room.skinCounter))
                }
            else ->
                Room(
                    roomId, true,
                    hashMapOf(User(userId, session) to State(skin = 0)),
                    CROSSBOW_RESPAWN_POINT
                ).also { ServerState.rooms[roomId] = it }
        }?.also {
            it.skinCounter += 1
        }
        broadcast(roomId, Message(userId, roomId, Connected))
        logger.info("connected: $userId to $roomId")
        logger.info("rooms: ${ServerState.rooms.map { it.key to it.value.users.map { it.key.userId } }}")
    }

    @OnMessage
    fun onMessage(
        session: Session,
        message: Message,
        @PathParam("userId") userId: String
    ) {
        val roomId = ServerState.mainService?.getRoomStrict(userId)!!._id
        val room = ServerState.rooms[roomId] ?: throw BadRequestException("No room found: $roomId")
        if (!room.running) return
        when (val event = message.event) {
            is PositionChanged -> movePlayer(room, userId, event)
            is PlayerAttacked -> attackPlayers(room, userId, event)
            is CrossBowPick -> pickCrossBow(room, userId, event)
            else -> {
            }
        }
        broadcast(roomId, message.copy(room = roomId))
    }

    @OnClose
    fun onClose(
        session: Session,
        @PathParam("userId") userId: String
    ) {
        val roomId = ServerState.mainService?.getRoomStrict(userId)!!._id
        val room = ServerState.rooms[roomId]
        room?.users?.remove(User(userId, session))
            .also { broadcast(roomId, Message(userId, roomId, Disconnected)) }
            .also { session.close() }
            .also {
                ServerState.mainService?.exitRoom(userId)
                room?.let { it.skinCounter -= 1 }
                if (roomIsEmpty(roomId)) {
                    ServerState.rooms.remove(roomId)
                    ServerState.mainService?.clearRoom(roomId)
                }
            }
        logger.info("disconnected: $userId from $roomId")
        logger.info("room size: ${ServerState.rooms.map { it.key to it.value.users.map { it.key.userId } }}")
    }

    private fun pickCrossBow(room: Room, userId: UserID, event: CrossBowPick) {
        if (room.crossBow == null) return

        room.getUserState(event.user)?.let { _ ->
            room.crossBow = null
            mutator.mutateState(room, event.user) { _, s -> s.crossBow = CROSSBOW_BOLT_COUNT }
            Timer("crossBowRespawn", true).schedule(CROSSBOW_RESPAWN_TIME) {
                room.crossBow = CROSSBOW_RESPAWN_POINT
                broadcast(room.roomId, Message("server", room.roomId, CrossBowRespawn(CROSSBOW_RESPAWN_POINT)))
            }
        }
    }

    private fun attackPlayers(room: Room, attacker: UserID, event: PlayerAttacked) {
        room.getUserState(attacker)?.also { ast ->
            val killed = HashSet<String>()
            if (event.weapon == Weapon.MELEE) {
                val hits = room.users.filter { (user, state) ->
                    val pos = when (ast.direction) {
                        Direction.RIGHT -> Position(ast.pos.pX, ast.pos.pY - PLAYER_ATTACK_RANGE * 0.5f)
                        Direction.LEFT  -> Position(ast.pos.pX - PLAYER_ATTACK_RANGE, ast.pos.pY - PLAYER_ATTACK_RANGE * 0.5f)
                        Direction.UP    -> Position(ast.pos.pX - PLAYER_ATTACK_RANGE * 0.5f, (ast.pos.pY - PLAYER_ATTACK_RANGE))
                        Direction.DOWN  -> Position(ast.pos.pX - PLAYER_ATTACK_RANGE * 0.5f, ast.pos.pY)
                    }
                    user.userId != attacker && huyabb(pos, state.pos, PLAYER_ATTACK_RANGE)
                }.keys.map { it.userId }

                mutator.mutateStates(room, hits.toSet()) { user, state ->
                    if (ast.alive && state.alive) {
                        state.alive = false
                        killed.add(user.userId)
                    }
                }

                mutator.mutateState(room, attacker) { _, state -> state.score += killed.size }?.also { (u, s) ->
                    broadcast(room.roomId, Message("server", room.roomId, PointsReceived(u.userId, killed.size, s.score)))
                    if (s.score >= ROOM_MAX_SCORE) {
                        onMatchFinish(room)
                    }
                }
                broadcast(room.roomId, Message("server", room.roomId, PlayersKilled(killed)))
                respawn(room, killed)
            } else {
                val state = room.getUserState(attacker)
                if (state != null && state.crossBow <= 0)
                    return
                mutator.mutateState(room, attacker) { _, s -> s.crossBow -= 1; }
                room.users
                    .filter { (user, state) -> user.userId != attacker && state.alive }
                    .map { (u, st) ->
                        val timeToKill =
                            sqrt((st.pos.pX - ast.pos.pX).pow(2) + (st.pos.pY - ast.pos.pY).pow(2)) / BOLT_MS
                        timeToKill to (u to st)
                    }.find { (timeToKill, ust) ->
                        val st = ust.second
                        val victimNextPos = predictPosition(st, PLAYER_MS, timeToKill)
                        val boltNextPos = predictPosition(ast, BOLT_MS, timeToKill)
                        victimNextPos.collides(boltNextPos, PLAYER_WIDTH, PLAYER_HEIGHT, BOLT_WIDTH, BOLT_WIDTH)
                    }?.let { (timeToKill, ust) ->
                        val killedId = ust.first.userId
                        val killedState = ust.second
                        val timeToKillMs = (timeToKill * 1000).toLong()

                        Timer("boltKill", true).schedule(timeToKillMs) {
                            mutator.mutateState(room, attacker) { _, s -> s.score += 1 }
                            mutator.mutateState(room, killedId) { _, s -> s.alive = false }
                            broadcast(room.roomId, Message("server", room.roomId, PointsReceived(killedId, 1, killedState.score)))
                            broadcast(room.roomId, Message("server", room.roomId, PlayersKilled(setOf(killedId))))
                            if (killedState.score >= ROOM_MAX_SCORE) {
                                onMatchFinish(room)
                            }
                            respawn(room, setOf(killedId))
                        }
                    }
            }
        }
    }

    private fun respawn(room: Room, usersId: Set<String>) {
        Timer("respawn", true).schedule(PLAYER_RESPAWN_TIME) {
            mutator.mutateStates(room, usersId) { _, state ->
                state.pos = RESPAWN_POINTS.random()
                state.alive = true
            }.also {
                val respawns = it.map { (u, s) -> u.userId to s.pos }.toMap()
                broadcast(room.roomId, Message("server", room.roomId, PlayersRespawn(respawns)))
            }
        }
    }

    private fun movePlayer(room: Room, userId: String, event: PositionChanged) {
        mutator.mutateState(room, userId) { _, state ->
            val collision = room.users.any { (otherUser, otherState) ->
                otherUser.userId != userId && otherState.pos.contains(event.pos, PLAYER_WIDTH, PLAYER_HEIGHT)
            }
            val newDirection =
                if (abs(event.speedX - state.speedX) > EPSILON || abs(event.speedY - state.speedY) > EPSILON) {
                    getDirection(event.speedX, event.speedY)
                } else {
                    state.direction
                }
            if (!collision) {
                val newX = event.pos.pX + (event.speedX * PLAYER_MS * MAGIC_PING)
                val newY = event.pos.pY + (event.speedY * PLAYER_MS * MAGIC_PING)
                state.pos = Position(newX, newY)
                state.speedX = event.speedX
                state.speedY = event.speedY
            }
            state.direction = newDirection
        }
    }

    private fun getDirection(speedX: Float, speedY: Float): Direction {
        val angle = atan2(speedY, speedX) * 180f / 3.141592653589793238462643383279502884f
        return when {
            angle > -135f && angle <= -45f -> Direction.UP
            angle > -45f && angle <= 45f -> Direction.RIGHT
            angle > 45f && angle <= 135f -> Direction.DOWN
            else -> Direction.LEFT
        }
    }

    private fun predictPosition(state: State, speed: Float, after: Float) =
        Position(
            state.pos.pX + (state.speedX * speed * after),
            state.pos.pY + (state.speedY * speed * after)
        )

    private fun onMatchFinish(room: Room) {
        room.running = false
        val stats = room.users.toList()
            .map { (user, state) -> user.userId to state.score }
            .toMap()

        stats.maxBy { it.value }?.let { (winnerId, _) ->
            broadcast(room.roomId, Message("server", room.roomId, MatchFinished(winnerId, stats)))
        }
    }
}