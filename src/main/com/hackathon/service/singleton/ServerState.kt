package com.hackathon.service.singleton

import com.hackathon.service.model.Room
import com.hackathon.service.common.RoomID
import com.hackathon.service.model.ROOM_MAX_PLAYERS
import com.hackathon.service.service.MainService

object ServerState {
    var mainService: MainService? = null

    val rooms = HashMap<RoomID, Room>()
}

fun roomIsEmpty(roomId: String) = ServerState.rooms[roomId]?.users?.isEmpty() ?: true

fun getRoomByUserId(userId: String): RoomID? {
    return ServerState.rooms.toList().find {
        it.second.users.any { (user, state) ->
            user.userId == userId
        }
    }?.first
}

fun joinAnyRoom(userId: String): Room? {
    return ServerState.rooms
        .filter { (_, room) -> room.users.size < ROOM_MAX_PLAYERS }
        .entries
        .firstOrNull()?.value
}
