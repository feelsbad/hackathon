package com.hackathon.service.model

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.hackathon.service.common.Score
import com.hackathon.service.common.UserID
import org.slf4j.LoggerFactory

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes(
    JsonSubTypes.Type(Connected::class),
    JsonSubTypes.Type(Disconnected::class),
    JsonSubTypes.Type(PlayerAttacked::class),
    JsonSubTypes.Type(PointsReceived::class),
    JsonSubTypes.Type(PositionChanged::class),
    JsonSubTypes.Type(MatchFinished::class),
    JsonSubTypes.Type(PlayersRespawn::class),
    JsonSubTypes.Type(CrossBowPick::class),
    JsonSubTypes.Type(CrossBowRespawn::class),
    JsonSubTypes.Type(PlayersKilled::class),
    JsonSubTypes.Type(Attack::class)
)
sealed class Event

data class StateUpdate(val states: Map<String, State>?) : Event()

data class PositionChanged(val pos: Position, val speedX: Float, val speedY: Float) : Event()
data class PlayerAttacked(val weapon: Weapon) : Event()
data class PlayersKilled(val users: Set<UserID>) : Event()
data class PlayersRespawn(val users: Map<UserID, Position>) : Event()
data class PointsReceived(val user: UserID, val received: Int, val total: Int) : Event()
data class MatchFinished(val winner: UserID, val stats: Map<UserID, Score>) : Event()
data class CrossBowRespawn(val pos: Position) : Event() // server
data class CrossBowPick(val user: UserID) : Event() // client
data class Attack(val pos: Position, val target: String, val range: Float) : Event()

object Connected : Event()
object Disconnected : Event()

enum class Direction {
    UP, DOWN, LEFT, RIGHT
}

fun isBeaten(p0: Position, p1: Position, range: Float, dir: Direction): Boolean {
    val half = range / 2
    return when (dir) {
        Direction.UP -> (p1.pX > p0.pX - half) && (p1.pX < p0.pX + half) && (p1.pY < p0.pY + range)
        Direction.DOWN -> (p1.pX > p0.pX - half) && (p1.pX < p0.pX + half) && (p1.pY < p0.pY - range)
        Direction.LEFT -> (p1.pY < p0.pY + half) && (p1.pY > p0.pY - half) && (p1.pX > p0.pX - range)
        Direction.RIGHT -> (p1.pY < p0.pY + half) && (p1.pY > p0.pY - half) && (p1.pX > p0.pX + range)
    }
}

fun huyabb(p0: Position, target: Position, w0: Float): Boolean {
    return (target.pX >= p0.pX - w0)
        && (target.pX <= p0.pX + w0)
        && (target.pY >= p0.pY - w0)
        && (target.pY <= p0.pY + w0)
}

fun aabb(p0: Position, p1: Position, w0: Float, h0: Float, w1: Float, h1: Float): Boolean {
    return (p0.pX <= p1.pX + w1) && (p0.pX + w0 >= p1.pX) && (p0.pY <= p1.pY + h1) && (p0.pY + h0 >= p1.pY)
}