package com.hackathon.service.model

import javax.websocket.Session

data class Room(
    val roomId: String,
    var running: Boolean,
    val users: HashMap<User, State>,
    var crossBow: Position? = null
) {
    var skinCounter = 0

    fun getUser(userId: String): User? =
        users.toList().firstOrNull { (user, _) -> user.userId == userId }?.first

    fun getUserState(userId: String): State? =
        users.toList().firstOrNull { (user, _) -> user.userId == userId }?.second

    fun getUsers(usersId: Set<String>): List<User> =
        users.filter { (user, _) -> user.userId in usersId }.map { it.key }
}

data class User(
    val userId: String,
    val session: Session
)
