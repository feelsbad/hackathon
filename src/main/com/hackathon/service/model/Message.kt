package com.hackathon.service.model

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.hackathon.service.common.RoomID
import com.hackathon.service.common.UserID
import org.slf4j.LoggerFactory
import javax.websocket.Decoder
import javax.websocket.Encoder
import javax.websocket.EndpointConfig

data class Message(
    val from: UserID,
    val room: RoomID?,
    val event: Event
)

class MessageEncoder : Encoder.Text<Message> {
    private val logger = LoggerFactory.getLogger(this::class.java)
    private val mapper = ObjectMapper().registerKotlinModule()

    override fun encode(message: Message): String = mapper.writeValueAsString(message)
    override fun destroy() {}
    override fun init(config: EndpointConfig) {}
}

class MessageDecoder : Decoder.Text<Message> {
    private val logger = LoggerFactory.getLogger(this::class.java)
    private val mapper = ObjectMapper().registerKotlinModule()

    override fun willDecode(message: String): Boolean =
        try {
            mapper.readValue<Message>(message)
            true
        } catch (e: Exception) {
            logger.error("Cannot decode message", e)
            false
        }

    override fun decode(message: String): Message = mapper.readValue(message)
    override fun destroy() {}
    override fun init(config: EndpointConfig) {}
}