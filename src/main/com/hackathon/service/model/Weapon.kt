package com.hackathon.service.model

/*
 *  Copyright (C) MyTona, LLC - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Mhs <maksim.barashkov@mytona.com> on 16/11/2019.
 */

enum class Weapon {
    MELEE,
    RANGE
}