package com.hackathon.service.model

/*
 *  Copyright (C) MyTona, LLC - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Mhs <maksim.barashkov@mytona.com> on 16/11/2019.
 */

const val PLAYER_MS = 600f
const val PLAYER_WIDTH = 90f
const val PLAYER_HEIGHT = 106f
const val PLAYER_ATTACK_RANGE = 160f
const val PLAYER_RESPAWN_TIME = 2000L

const val ROOM_MAX_SCORE = 20
const val ROOM_MAX_PLAYERS = 4

const val BOLT_MS = 1200f
const val BOLT_WIDTH = 28f
const val MAGIC_PING = 0

const val EPSILON = 0.000001f

val RESPAWN_POINTS = listOf(
    Position(148f, 281f),
    Position(2583f, 272f),
    Position(171f, 2412f),
    Position(2550f, 2409f)
)

const val CROSSBOW_BOLT_COUNT = 10
const val CROSSBOW_RESPAWN_TIME = 20000L
val CROSSBOW_RESPAWN_POINT = Position(0f, 0f)