package com.hackathon.service.model

data class State(
    var alive: Boolean = true,
    var score: Int = 0,
    var pos: Position = Position(0f, 0f),
    var speedX: Float = 0f,
    var speedY: Float = 0f,
    var direction: Direction = Direction.UP,
    var crossBow: Int = 0,
    var skin: Int
)

data class Position(
    val pX: Float,
    val pY: Float
) {
    fun collides(o: Position, w: Float, h: Float, ow: Float, oh: Float): Boolean {
        return ((pX - w/2) < (o.pX + ow/2) && (pX + w/2) > (o.pX - ow/2))
            && ((pY - h/2) < (o.pY + oh/2) && (pY + h/2) > (o.pY - oh/2))
    }

    fun contains(o: Position, width: Float, height: Float): Boolean {
        val halfW = width / 2
        val halfH = height / 2
        return (pX >= o.pX - halfW) && (pX <= o.pX + halfW) && (pY >= o.pY - halfH) && (pY <= o.pY + halfH)
    }
}