package com.hackathon.service.service

import com.hackathon.service.common.RoomID
import com.hackathon.service.common.Score
import com.hackathon.service.common.UserID
import com.hackathon.service.model.ROOM_MAX_PLAYERS
import java.util.UUID

class MainService {
    companion object {
        private val roomsDB = HashMap<RoomID, RoomDB>()
        private val topDB = HashMap<UserID, TopRecord>()
    }

    data class TopRecord(
        val nickname: String,
        val score: Score
    )

    data class RoomDB(
        val _id: String = UUID.randomUUID().toString(),
        val users: HashSet<String>
    )

    fun getRoomStrict(userId: String): RoomDB? =
        roomsDB.entries.find { it.value.users.contains(userId) }?.value

    fun getRoom(userId: String): RoomDB { // lul
        return getRoomStrict(userId)
            ?: roomsDB.entries
                .find { (_, room) -> room.users.size < ROOM_MAX_PLAYERS }
                ?.let { (_, room) -> room.users.add(userId); room }
            ?: let { RoomDB(users = hashSetOf(userId)).also { roomsDB[it._id] = it } }
    }

    fun getRooms() = roomsDB

    fun exitRoom(userId: String): RoomDB? {
        return getRoomStrict(userId)?.let {
            it.users.remove(userId)
            it
        }
    }

    fun getTop() = topDB.entries.sortedByDescending { it.value.score }.map { it.value }

    fun setScore(userId: String, score: Int, nickname: String) {
        topDB[userId] = TopRecord(nickname, score)
    }

    fun clearRoom(roomID: RoomID) {
        roomsDB.remove(roomID)
    }

    fun cleanup() {
        roomsDB.clear()
        topDB.clear()
    }
}