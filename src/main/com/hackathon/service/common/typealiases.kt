package com.hackathon.service.common

typealias UserID = String
typealias RoomID = String
typealias Score = Int