package com.hackathon.service

import com.google.inject.AbstractModule
import com.google.inject.Provides
import com.google.inject.Singleton
import io.dropwizard.setup.Environment

class AppModule(private val configuration: Configuration, private val environment: Environment) : AbstractModule() {

    override fun configure() {
    }

    @Provides
    @Singleton
    fun configuration() = configuration

    @Provides
    fun objectMapper() = environment.objectMapper

//    @Provides
//    @javax.inject.Singleton
//    fun provideJerseyClient(): Client = JerseyClientBuilder(environment).using(configuration.jerseyClient).build("client")
}