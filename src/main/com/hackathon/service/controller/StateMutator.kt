package com.hackathon.service.controller

import com.hackathon.service.common.UserID
import com.hackathon.service.model.Room
import com.hackathon.service.model.State
import com.hackathon.service.model.User

/*
 *  Copyright (C) MyTona, LLC - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Mhs <maksim.barashkov@mytona.com> on 17/11/2019.
 */

class StateMutator {
    fun mutateStates(room: Room, usersId: Set<UserID>, mutator: (User, State) -> Unit): List<Pair<User, State>> =
        room.getUsers(usersId)
            .let { users -> room.users.filter { it.key in users } }
            .map { (user, state) -> mutator(user, state); user to state }

    fun mutateState(room: Room, userId: UserID, mutator: (User, State) -> Unit): Pair<User, State>? =
        room.getUser(userId)?.let { user ->
            val state = room.users.getValue(user)
            mutator(user, state)
            user to state
        }
}