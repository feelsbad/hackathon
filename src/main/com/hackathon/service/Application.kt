package com.hackathon.service

import ch.qos.logback.classic.LoggerContext
import ch.qos.logback.classic.util.ContextInitializer
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.google.inject.Guice
import com.hackathon.service.api.FooWebSocket
import com.hackathon.service.api.MainResource
import com.hackathon.service.api.MainWebSocket
import com.hackathon.service.singleton.ServerState
import com.hackathon.service.service.MainService
import io.dropwizard.setup.Bootstrap
import io.dropwizard.setup.Environment
import io.dropwizard.websockets.WebsocketBundle
import io.federecio.dropwizard.swagger.SwaggerBundle
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration
import org.slf4j.LoggerFactory
import java.nio.file.Paths

class Application : io.dropwizard.Application<Configuration>() {
    private val webSocketBundle: WebsocketBundle = WebsocketBundle(FooWebSocket::class.java)

    override fun initialize(bootstrap: Bootstrap<Configuration>) {
        bootstrap.objectMapper.registerModule(KotlinModule())
        bootstrap.addBundle(webSocketBundle)
        bootstrap.addBundle(object : SwaggerBundle<Configuration>() {
            override fun getSwaggerBundleConfiguration(configuration: Configuration): SwaggerBundleConfiguration {
                return SwaggerBundleConfiguration().apply {
                    resourcePackage = "com.hackathon.service.api"
                }
            }
        })
    }

    override fun run(configuration: Configuration, environment: Environment) {
        setUpLogging(configuration.project)

        val injector = Guice.createInjector(AppModule(configuration, environment))
        val mainService = injector.getInstance(MainService::class.java)
        ServerState.mainService = mainService // AAAAAA

        environment.jersey().apply {
            register(injector.getInstance(MainResource::class.java))
        }
        webSocketBundle.addEndpoint(MainWebSocket::class.java)
    }

    private fun setUpLogging(project: String) {
        System.setProperty("project", project.toLowerCase())

        val factory = LoggerFactory.getILoggerFactory()
        val context = factory as LoggerContext
        context.reset()

        val contextInitializer = ContextInitializer(context)
        contextInitializer.autoConfig()
    }

    companion object {

        @Throws(Exception::class)
        @JvmStatic
        fun main(args: Array<String>) {
            val configPath = Paths.get(args[1])
            val logbackConfigPath = configPath.parent.resolve("logback.xml")

            System.setProperty("logback.configurationFile", logbackConfigPath.toString())
            Application().run(*args)
        }
    }
}